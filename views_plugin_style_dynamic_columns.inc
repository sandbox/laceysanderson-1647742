<?php

// $Id$

/**
 * @file 
 * This file describes the style plugin used to provide
 * the administration menu for dynamic columns.
 * 
 * Module Description: This module implements a
 *   style plugin for views2 which allows any field
 *   to be designated as available for the user to 
 *   add/remove; whether to show the field by default
 *   can also be specified.
 */

class views_plugin_style_dynamic_columns extends views_plugin_style_table {

  /*****************************************************
   * Purpose: Defines the defaults for the form elements
   *   defined in function options_form()
   *
   * @return the default options to be used for the options
   *   form which is the administrative form (accessed by clicking
   *   the gear beside the plugin name)
   */
  function option_definition() {
    $options = parent::option_definition();
    
    
    return $options;
  }

  /*****************************************************
   * Purpose: Builds the options form provided when you
   *   select the gear beside the style plugin
   *
   * The form doesn't need to be returned because it is edited directly
   * due to the &$form and stored within the object
   *
    * @params form: form array as described for drupal form api
   * @params form_state: form_state array as described for drupal form api
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $form['#theme'] = 'views_ui_style_plugin_dynamic_columns';
    
    $handlers = $this->display->handler->get_handlers('field');
    if (empty($handlers)) {
      $form['error_markup'] = array(
        '#value' => t('You need at least one field before you can configure your table settings'),
        '#prefix' => '<div class="error form-item description">',
        '#suffix' => '</div>',
      );
      return;
    }

    $columns = $this->sanitize_columns($this->options['columns']);

    $form['description_markup'] = array(
      '#prefix' => '<div class="description form-item">',
      '#suffix' => '</div>',
      '#value' => t('Check the Allow +/- box to allow the user to select whether to show/hide this field. If you do, check the Show box to indicate whether you want this field shown by default or not. Check the sortable box to make that column click sortable, and check the default sort radio to determine which column will be sorted by default, if any. You may control column order and field labels in the fields section.'),

    );
    // Create an array of allowed columns from the data we know:
    $field_names = $this->display->handler->get_field_labels();
    
    foreach ($columns as $field => $column) {
      $form['info'][$field]['add_remove'] = array(
        '#type' => 'checkbox',
        '#default_value' => !empty($this->options['info'][$field]['add_remove']),
      );
      
      $form['info'][$field]['hide'] = array(
        '#type' => 'checkbox',
        '#default_value' => $this->options['info'][$field]['hide'],
      );
    
    }
  }
}