<?php

// $Id$

/**
 * @file
 * This file is required for views integration and
 * contains many of the views hook definitions
 */
 
// Requirement of these paths is needed to ensure that
// the style plugin defined can inherit from the correct
// views style plugins
$views_path = drupal_get_path('module', 'views');
require($views_path . '/plugins/views_plugin_style.inc');
require($views_path . '/plugins/views_plugin_style_table.inc');

/*****************************************************
 * Implements hook_views_plugins()
 *
 * Purpose: To register a custom views style plugin
 *
 * @return array to register views plugins
 */
function dynamic_views_columns_views_plugins() {
  return array(
    'style' => array(
      'views_dynamic_columns' => array(
        'title' => t('Dynamic Columned Table'),
        'theme' => 'views_dynamic_columns',
        'help' => t('Allows the user to choose which columns to display.'),
        'handler' => 'views_plugin_style_dynamic_columns',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',       
      ),
    ),
  );
}
