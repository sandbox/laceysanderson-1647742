$Id$

README.txt
==========

This module provides a views style plugin called Dynamic Columned Table. This 
style plugin formats the view the same as the default table style plugin.
However, it adds some extra administration options (accessed by clicking
the gears next to the plugin name) including the ability to indicate which
columns should be shown by default and which columns the user is allowed
to add/remove themselves. 

Users can add/remove columns through a form with two select lists and submit 
buttons. The select list for adding columns lists all columns which the 
view creator indicated the user could add/remove that haven't already been added
to the view. The select list for removing columns lists only columns which are
already displayed in the view and the view creator indicated the user could
control.

This module follows theme-ing conventions provided by views by using the same
id tags to format the form as is used for the exposed filter forms. This ensures
that the exposed for which allows users to control columns visually "fits"
with the rest of the view.

Security Notes:
-------------------------------------
Since only fields which have been added to the view by the view creator can
be added/removed from the view by the user, no sensitive information in other
fields (not added to the view by the iew creator) can be accessed.

Furthermore, since the view creator has to indicate which fields the user
can control and all fields are disabled for user control by default, the user
can only be given extra control if the view creator specifically grants it.

One more important note is that field names listed in the path will only be
shown in the view if they are in the list of view creator selected fields under
user control.

A Note to New Users:
---------------------------------------
Once this module has been enabled, the dynamic columns functionality can be
added to any exhisting or newly created view by changing the Style (found
under the heading of Basic Settings in the Views UI) to "Dynamic Columned
Table". 

Once fields have been added to the view, you can choose to allow the user
to add/remove fields by selecting the advanced options for the style (click
the gear beside the style name) and checking the Allow +/- checkbox for that
field. You can also select whether this field should be displayed in the view
by default by selecting the Show checkbox. The user will be able to remove
this field after any other of the available columns have been added.