<?php

// $Id$

/**
 * @file views-dynamic-columns.tpl.php
 * Template to display a view as a table with dynamic columns
 * which can be added/removed by the user.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $dynamic_columns: An array of variables to be used for managing dynamic columns
 *     - fields_not_added: an array of fields which have been selected by 
 *         the view creator to be available to add to the view but
 *         have not been. This array is used to populate the Select List
 *         of Fields the user can Add.
 *     - fields_to_remove: an array of fields which have been selected by 
 *         the view creator to be available to remove and are currently 
 *         displayed in the view
 *     - fields_to_show: an array of fields the views creator has not given
 *         the user the choice to remove and fields the user has added
 *     - default_fields: an array of fields that the views creator
 *         has indicated should be shown by default but can be removed by
 *         the user
 * @ingroup views_templates
 */
?>

<?php if (!empty($title)) : ?>
  <caption><?php print $title; ?></caption>
<?php endif; ?>

<!-- Display the form to the user for adding/removing columns -->
<div class='views-exposed-form'>
  <div class='views-exposed-widgets clear-block'>
    <?php if (sizeof($dynamic_columns['fields_not_added'])) { ?>
      <?php print drupal_get_form('dynamic_views_columns_add_column_form', $dynamic_columns['fields_not_added'], $dynamic_columns['default_fields']); ?>
    <? } ?>
    <?php if (sizeof($dynamic_columns['fields_to_remove'])) { ?>
      <?php print drupal_get_form('dynamic_views_columns_remove_column_form', $dynamic_columns['fields_to_remove']); ?>
    <? } ?>
  </div>
</div>
<br />

<!-- Generation of the View Table -->
<table class="<?php print $class; ?>">
  <thead>
    <tr>
      <!-- Only if the field is indicated to be shown should it be shown -->
      <?php foreach ($header as $field => $label): ?>
        <?php if (!empty($dynamic_columns['fields_to_show'][$field])) { ?>
          <th class="views-field views-field-<?php print $fields[$field]; ?>">
            <?php print $label; ?>
          </th>
        <?php } ?>
      <?php endforeach; ?>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($rows as $count => $row): ?>
      <tr class="<?php print implode(' ', $row_classes[$count]); ?>">
        <?php foreach ($row as $field => $content): ?>
          <!-- Only if the field is indicated to be shown should it be shown -->
          <?php if (!empty($dynamic_columns['fields_to_show'][$field])) { ?>
            <td class="views-field views-field-<?php print $fields[$field]; ?>">
              <?php print $content; ?>
            </td>
          <?php } ?>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
